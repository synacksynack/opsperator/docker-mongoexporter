# k8s Prometheus MongoDB Exporter

Historiclly based on https://github.com/dcu/mongodb_exporter. Moved to
https://github.com/percona/mongodb_exporter as of MongoDB4 4.

Build with:

```
$ make build
```

If you want to try it quickly on your local machine after make, run:

```
$ make run
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```
