SKIP_SQUASH?=1
FRONTNAME=opsperator
IMAGE=opsperator/mongoexporter
-include Makefile.cust

.PHONY: build
build:
	@@SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: clean
clean:
	@@docker rm -f testmongodb || true
	@@docker rm -f testmgxp || true

.PHONY: testmg
testmg:
	@@docker rm -f testmongodb || true
	@@docker run \
	    -e MONGODB_ADMIN_PASSWORD=secret \
	    -e MONGODB_DATABASE=testdb \
	    -e MONGODB_USER=myusr \
	    -e MONGODB_PASSWORD=testpw \
	    -p 27017:27017 \
	    --name testmongodb \
	    -d opsperator/mongodb


.PHONY: testmgok
testmgok:
	@@if ! docker exec testmongodb /bin/sh -c 'mongo 127.0.0.1:27017/$$MONGODB_DATABASE -u $$MONGODB_USER -p $$MONGODB_PASSWORD --eval="quit()" && echo OK' | grep OK; then \
	    echo MongoDB KO; \
	    exit 1; \
	fi; \
	echo MongoDB OK

.PHONY: test
test:
	@@if ! make testmgok; then \
	    make testmg; \
	    sleep 120; \
	fi
	@@docker rm -f testmgxp || true
	@@mgip=`docker inspect testmongodb | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	docker run --name testmgxp \
	    -e DEBUG=yay \
	    -p 9113:9113 \
	    -it $(IMAGE) \
	    "--mongodb.uri=mongodb://myusr:testpw@$$mgip:27017/testdb?ssl=false"

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in service statefulset; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc create -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "MONGO_EXPORTER_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "MONGO_EXPORTER_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc create -f-

.PHONY: ocdemopersistent
ocdemopersistent: ocbuild
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc create -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
