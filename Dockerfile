FROM docker.io/golang:1.16 AS builder

ARG MGXP_VERSION=0.20.7

# MongoDB Exporter image for OpenShift Origin

RUN set -x \
    && mkdir -p /go/src/github.com/percona \
    && cd /go/src/github.com/percona \
    && if ! git clone -b release-$MGXP_VERSION https://github.com/percona/mongodb_exporter; then \
	if ! git clone -b v$MGXP_VERSION https://github.com/percona/mongodb_exporter; then \
	    if ! git clone -b $MGXP_VERSION https://github.com/percona/mongodb_exporter; then \
		echo Failed cloning with ref: $MGXP_VERSION; \
		exit 1; \
	    fi; \
	fi; \
    fi

WORKDIR /go/src/github.com/percona/mongodb_exporter

RUN make build

FROM docker.io/debian:buster-slim

ARG MGXP_VERSION=0.20.7

LABEL io.k8s.description="MongoDB Prometheus Exporter Image." \
      io.k8s.display-name="MongoDB Prometheus Exporter" \
      io.openshift.expose-services="9113:http" \
      io.openshift.tags="prometheus,exporter,mongodb" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-mongoexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$MGXP_VERSION"

COPY --from=builder /go/src/github.com/percona/mongodb_exporter/mongodb_exporter /bin/mongodb_exporter
COPY config/* /

ENV DEBIAN_FRONTEND=noninteractive

RUN set -x \
    && echo "# Install Dumb-init" \
    && apt-get update \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && apt-get -y install wget \
    && chown 1001:root /tmp \
    && chmod 775 /tmp \
    && echo "# Cleaning Up" \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rvf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ENTRYPOINT [ "/run_mongodb_exporter" ]
USER 1001
